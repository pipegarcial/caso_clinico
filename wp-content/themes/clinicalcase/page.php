<!-- ================== -->
<!-- Header of template -->
<?php
get_header();
?>
  <!-- ================ -->
  <!-- Body of template -->

  <body>

    <header>
      <nav class=" main-nav">
        <div class="container">
          <div class="nav-wrapper">
            <a href="<?php echo bloginfo('url') ?>" class="brand-logo">
<img class="responsive-img"
src="<?php echo get_bloginfo('template_directory').'/assets/images/clini-case.png'?>" alt="logo" width="180">
</a>
            <!--<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>-->
            <ul class="right hide-on-med-and-down">
              <li>
                <img class="margin-right-1 margin-top-05" src="<?php echo get_bloginfo('template_directory').'/assets/images/icesi.png'?>" alt="logo" width="130">
              </li>
              <li>
                <img class="margin-top-05" src="<?php echo get_bloginfo('template_directory').'/assets/images/valle-lili.png'?>" alt="logo" width="130">
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
    <section class="margin-top-5 container">
      <?php
if (have_posts()) :
    while (have_posts()) :
        the_post();
    ?>
        <article class="row">
          <h2 class="titulo col s12 m12 l12 center-align uppercase"><?php bloginfo('name')?></h2>
          <?php the_content()?>
        </article>
        <article class="row">
          <div class="s12 m12 l12 center-align">
            <a href="<?php echo bloginfo('url') .'/el-caso-clinico/' ?>" class="waves-effect waves-light btn-large azul"><i class="material-icons right">play_circle_filled</i>Comenzar</a>
          </div>
        </article>
        <article class="row">
          <p class="col s12 m12 l12 center-align line-height-12px">
            * Datos y eventos descritos, reflejan de manera fidedigna los hechos
            <br> acontecidos durante la hospitalización extrainstitucional de la paciente.
          </p>
        </article>
        <?php
    endwhile;
    endif;
    ?>
    </section>



    <!-- ================= -->
    <!--Footer of template -->
    <?php
    get_footer();
    ?>