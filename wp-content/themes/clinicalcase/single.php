<!-- ================== -->
<!-- Header of template -->
<?php
            $current_page = get_queried_object('ID');
            $category     = get_the_category($current_page->ID);
            $query = new WP_Query( 
                array(
                    'category_name' => 'history',
                    'order'         => 'asc',
                    'post_type'     => 'post',
                    'post_status'   => 'publish',
                )
            );   

   //-----------------------------------         
   //Logic to save history slide visited
  // echo "<h1>".intval($current_page->ID) ."</h1>";
    if($current_page->ID > intval($_COOKIE["id_post_history"])){
         setcookie("id_post_history",$current_page->ID,time()+3600,"/");
    }
    //-----------------------------------         
   //Getting header fragment
    get_header();
    
    ?>
<!-- ================ -->
<!-- Body of template -->
<body>
    <header>
        <nav class=" main-nav">
            <div class="container">
                <div class="nav-wrapper">
                    <a href="<?php echo bloginfo('url') ?>" class="brand-logo">
                        <img class="responsive-img" 
                        src="<?php echo get_bloginfo('template_directory').'/assets/images/clini-case.png'?>" alt="logo" width="180">
                    </a> 
                    <!--<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>-->
                    <ul class="right hide-on-med-and-down">
                        <li>
                             <img class="margin-right-1 margin-top-05" 
                        src="<?php echo get_bloginfo('template_directory').'/assets/images/icesi.png'?>" alt="logo" width="130">
                        </li>
                        <li>
                        <img class="margin-top-05" 
                            src="<?php echo get_bloginfo('template_directory').'/assets/images/valle-lili.png'?>" alt="logo" width="130">
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <section class="container">
        <!--Inicio loop WP-->
        <?php  
            if ($query->have_posts()) :
            while (have_posts()) :
            the_post();
            ?>
        <!--Title post and buttons fixed md -->
        <article class="row margin-top-5 hide-on-small-only">
            <div class="col s6 left-align">
                <h4 class="margin-0"><?php the_title();?></h4>
            </div>
            <div class="col s6 right-align">
                <!--The btn lab is available since post 90-->
                <?php
                if($current_page->ID>=90){
                ?>
                  <a class="waves-effect waves-light btn"  href="#modal-lab"><i class="material-icons left" >bubble_chart</i>laboratorios</a>
                <?php
                  }
                ?>
                <a class="waves-effect waves-light btn" href="#modal-ant"><i class="material-icons left">library_books</i>antecedentes</a>
            </div>
        </article>
        <!--Close title post and buttons fixed md -->
        <!-- ==================================== -->

        <!--Title post and buttons fixed sm -->
         <article class="row margin-top-5 hide-on-med-and-up">
            <div class="col s12 center-align">
                <h5 class="margin-0"><?php the_title();?></h5>
            </div>
            <div class="col s12 center-align margin-top-2">
                 <!--The btn lab is available since post 90-->
                <?php
                if($current_page->ID>=90){
                ?>
                <a class="waves-effect waves-light btn col s12 margin-top-1" href="#modal-lab"><i class="material-icons left">bubble_chart</i>laboratorio</a>
                <?php
                  }
                ?>
                <a class="waves-effect waves-light btn col s12 margin-top-1"href="#modal-ant"><i class="material-icons left">library_books</i>antecedentes</a>
            </div>
        </article>
        <!-- Close title post and buttons fixed md -->
        <!-- ==================================== -->
        <!--Content post-->
        <artice class="col s8 offset-s2 m6 offset-m3 the-content-article center-align">
            <?php
            if($current_page->ID === 42){
                 get_template_part('./templates_fragments/cards_ef');
            }else{
                the_content();
            }
            ?>
        </artice>

        <?php
            endwhile;
            endif;
            ?>
        <!--End loop WP-->
    </section>
    <!-- ================= -->
    <!--Footer of template -->
    <?php
        include(locate_template('footer.php'));
    ?>

