<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- ================ -->
    <!--Title project-->
    <title>
        <?php bloginfo('title'); ?>
    </title>
     <!-- ================ -->
     <!--Let browser know website is optimized for mobile-->
    <?php 
        wp_head();
    ?>
    <!-- ================ -->
    <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- ================ -->
    <!--CSS-->
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Main CSS WP styles.css WP head space-->
    <?php 
        wp_head();
    ?>
    <!-- ================ -->
    <!--JS-->
</head>
