<?php

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_theme_support( 'post-thumbnails' ); 
/**
* Adding styles of template
*/
function addStyles() {
    wp_enqueue_style('style', get_bloginfo('template_directory').'/style.css');
    wp_enqueue_style('materalize',get_bloginfo('template_directory').'/node_modules/materialize-css/dist/css/materialize.min.css');
    wp_enqueue_style('main',get_bloginfo('template_directory').'/assets/css/main.css');
}
add_action('wp_head', 'addStyles');
//===================================
/**
* Adding js of template
*/
function addJs() {
    //Jquery
    wp_enqueue_script('jquery', get_bloginfo('template_directory').'/node_modules/jquery/dist/jquery.min.js');
    //Materialize
    wp_enqueue_script('materialize', 
    get_bloginfo('template_directory').'/node_modules/materialize-css/dist/js/materialize.min.js');
    //Main JS
    wp_enqueue_script('main', get_bloginfo('template_directory').'/assets/js/main.js');

}
add_action('wp_footer', 'addJs');

?>