<?php
get_template_part('./templates_fragments/modals');
?>

  </main>

  <footer class="z-depth-5">
    <div class="container">
      <!-- ==================================== -->
      <!--Handler buttons paginator-->
      <?php
            if($category[0]->name === 'history'){
                get_template_part('./templates_fragments/handler_buttons_history');
            }
        ?>
        <section>

        </section>
    </div>

  </footer>


  <!-- ================ -->
  <!-- WP footer space -->
  <?php
wp_footer();
?>
    </body>

    </html>