<article class="row center-align">

  <div class="input-field col s12">

    <select onchange="location = this.value;">
      <option value="" disabled selected>Seleccione la sección que desee revisar de nuevo</option>
      <?php

$current_page = get_queried_object('ID');
$category     = get_the_category($current_page->ID);


$post_ids = range(1, intval($_COOKIE["id_post_history"]));

$all_posts = get_posts( array(
'post__in' => $post_ids,
'category_name' => 'history',
'order'         => 'asc',
'post_type'     => 'post',
'post_status'   => 'publish',
'posts_per_page'=> 14
) );



if ( $all_posts ) {
    foreach ( $all_posts as $post ) :
    setup_postdata( $post ); ?>
        <option value="<?php the_permalink();?>">
          <?php the_title(); ?>
        </option>
        <?php
    endforeach;
    wp_reset_postdata();
}?>
    </select>
  </div>
  <div class="col s12">
    <!--========================-->
    <!--Paginator prev -->
    <?php
$prev_post = get_previous_post('history');
if (!empty( $prev_post )): ?>
      <a href="<?php echo $prev_post->guid ?>" class="btn-floating btn-large waves-effect waves-light "><i class="material-icons">keyboard_arrow_left</i></a>
      <?php endif
?>
        <!--========================-->
        <!--Paginator go home -->
        <a href="<?php echo bloginfo('url') ?>" class="btn-floating btn-large waves-effect waves-light "><i class="material-icons">home</i></a>
        <!--========================-->
        <!--Paginator next -->
        <?php
$next_post = get_next_post();
if (!empty( $next_post )): ?>
          <a href="<?php echo $next_post->guid ?>" class="btn-floating btn-large waves-effect waves-light "><i class="material-icons">keyboard_arrow_right</i></a>
          <?php endif
?>
  </div>
</article>