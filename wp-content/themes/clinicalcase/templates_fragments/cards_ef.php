  <!--Starting row-->
            <div class="row">
                <!--1 ASPECTO GENERAL-->
                <div class=" col s12 m4 padding-l-r-1">
                    <div class=" col s12 m12  card-custom z-depth-2">
                        <figure class="col s12 fis-img-wrapper">
                            <img src="http://localhost/trabajos/clinical_case/wp-content/uploads/2017/05/general.svg" width="80">
                        </figure>
                        <div class="col s12 title-card margin-top 1 center-align">
                            <h5 class="card-title">General</h5>
                        </div>
                        <div class="col s12 card-description">
                            <p class="margin-top-1">
                                Paciente ingresa por sus propios medios, luce en aceptables condiciones generales, sin signos de dificultad respiratoria
                                Signos vitales: T°:37.2°C TA: 100/66 mmHg TAM: 77 mmHg FC:90 l.p.m FR:18 r.p.m SATO2: 99% FIO2: 0.21 PESO: 64 Kg
                            </p>
                        </div>
                    </div>
                </div>
                 <!--2  CABEZA Y CUELLO -->
                <div class=" col s12 m4 padding-l-r-1">
                    <div class=" col s12 m12  card-custom z-depth-2">
                        <figure class="col s12 fis-img-wrapper">
                            <img src="http://localhost/trabajos/clinical_case/wp-content/uploads/2017/05/cabezaycuello.svg" width="80">
                        </figure>
                        <div class="col s12 title-card margin-top 1 center-align">
                            <h5 class="card-title">CABEZA Y CUELLO</h5>
                        </div>
                        <div class="col s12 card-description">
                            <p class="margin-top-1">
                                Cuello móvil y no doloroso, sin adenopatías ni masas palpables, sin soplos carotideos. Ingurgitación yugular a 45°
                            </p>

                             <p class="margin-top-1">
                               mucosas y conjuntivas semipálidas y húmedas, escleras anictéricas.
                            </p>
                        </div>
                    </div>
                </div>
                <!--4 RESPIRATORIO-->
                <div class=" col s12 m4 padding-l-r-1">
                    <div class=" col s12 m12  card-custom z-depth-2">
                        <figure class="col s12 fis-img-wrapper">
                            <img src="<?php echo bloginfo('url') . '/wp-content/uploads/2017/05/respiratorio.svg'?>" width="80">
                        </figure>
                        <div class="col s12 title-card margin-top 1 center-align">
                            <h5 class="card-title">RESPIRATORIO</h5>
                        </div>
                        <div class="col s12 card-description">
                            <p class="margin-top-1">
                               Murmullo vesicular presente en ambos campos pulmonares, no se auscultan ruidos sobreagregados.
                            </p>
                        </div>
                    </div>
                </div>
            <!--Close row-->
            </div>

             <!--Starting row-->
            <div class="row">
                 <!--5 GASTROINTESTINAL-->
                <div class=" col s12 m4 padding-l-r-1">
                    <div class=" col s12 m12  card-custom z-depth-2">
                        <figure class="col s12 fis-img-wrapper">
                            <img src="<?php echo bloginfo('url') . '/wp-content/uploads/2017/05/abdomen.svg'?>" width="80">
                        </figure>
                        <div class="col s12 title-card margin-top 1 center-align">
                            <h5 class="card-title">GASTROINTESTINAL</h5>
                        </div>
                        <div class="col s12 card-description">
                            <p class="margin-top-1">
                               Peristaltismo presente, abdomen blando y depresible, no dolor a la palpación ni signos de irritación peritoneal, no se palpan masas ni
Visceromegalias.
                            </p>
                        </div>
                    </div>
                </div>
                <!--6 GINECOBSTÉTRICO -->
                <div class=" col s12 m4 padding-l-r-1">
                    <div class=" col s12 m12  card-custom z-depth-2">
                        <figure class="col s12 fis-img-wrapper">
                            <img src="<?php echo bloginfo('url') . '/wp-content/uploads/2017/05/gineco.svg'?>" width="80">
                        </figure>
                        <div class="col s12 title-card margin-top 1 center-align">
                            <h5 class="card-title">GINECOBSTÉTRICO</h5>
                        </div>
                        <div class="col s12 card-description">
                            <p class="margin-top-1">
                                Útero grávido reactivo leve, altura uterina de 25 cm, movimientos fetales presentes, FCF: 156 l.p.m, situación longitudinal, posición dorso izquierdo, presentación cefálica, no actividad uterina palpable. TV: cervix posterior, blando, corto, cerrado.
                            </p>
                        </div>
                    </div>
                 <!--Close div with padding-->
                </div>
                  <!--7 RENAL -->
                <div class=" col s12 m4 padding-l-r-1">
                    <div class=" col s12 m12  card-custom z-depth-2">
                        <figure class="col s12 fis-img-wrapper">
                            <img src="<?php echo bloginfo('url') . '/wp-content/uploads/2017/05/renal.svg'?>" width="80">
                        </figure>
                        <div class="col s12 title-card margin-top 1 center-align">
                            <h5 class="card-title">RENAL</h5>
                        </div>
                        <div class="col s12 card-description">
                            <p class="margin-top-1">
                                Diuresis espontanea, no cuantificada.
                            </p>
                        </div>
                    </div>
                </div>
            <!--Close row-->
            </div>

             <!--Starting row-->
            <div class="row">
              
                 <!--8 EXTREMIDADES-->
                <div class=" col s12 m4 padding-l-r-1">
                    <div class=" col s12 m12  card-custom z-depth-2">
                        <figure class="col s12 fis-img-wrapper">
                            <img src="<?php echo bloginfo('url') . '/wp-content/uploads/2017/05/extremidades.svg'?>" width="80">
                        </figure>
                        <div class="col s12 title-card margin-top 1 center-align">
                            <h5 class="card-title">EXTREMIDADES</h5>
                        </div>
                        <div class="col s12 card-description">
                            <p class="margin-top-1">
                                Sin edema, móviles, pulsos ++/++ simétricos en todas las extremidades, llenado capilar <2 seg.
                            </p>
                        </div>
                    </div>
                </div>
                <!--9 NEUROLÓGICO -->
                <div class=" col s12 m4 padding-l-r-1">
                    <div class=" col s12 m12  card-custom z-depth-2">
                        <figure class="col s12 fis-img-wrapper">
                            <img src="<?php echo bloginfo('url') . '/wp-content/uploads/2017/05/neuro.svg' ?>" width="80">
                        </figure>
                        <div class="col s12 title-card margin-top 1 center-align">
                            <h5 class="card-title">NEUROLÓGICO</h5>
                        </div>
                        <div class="col s12 card-description">
                            <p class="margin-top-1">
                                Alerta y orientada en las tres esferas, sin focalización, paresia parcial del III nervio craneal izquierdo, paresia del IV nervio craneal izquierdo, ptosis palpebral 
                            </p>
                        </div>
                    </div>
                    <!--=====================================-->
                 <!--Close div with padding-->
                </div>
                <!--=====================================-->
                   <!--10 PIEL -->
                <div class=" col s12 m4 padding-l-r-1">
                    <div class=" col s12 m12  card-custom z-depth-2">
                        <figure class="col s12 fis-img-wrapper">
                            <img src="<?php echo bloginfo('url') . '/wp-content/uploads/2017/05/piel.svg' ?>" width="80">
                        </figure>
                        <div class="col s12 title-card margin-top 1 center-align">
                            <h5 class="card-title">PIEL</h5>
                        </div>
                        <div class="col s12 card-description">
                            <p class="margin-top-1">
                                Sin lesiones visibles.
                            </p>
                        </div>
                    </div>
                    <!--=====================================-->
                 <!--Close div with padding-->
                </div>
            <!--Close row-->
            </div>
            <!--=====================================-->