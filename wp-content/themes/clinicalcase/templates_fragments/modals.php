<!-- Modal Structure Lab -->
<div id="modal-lab" class="modal modal-fixed-footer">
  <div class="modal-content center-align">
    <h4>LABORATORIO</h4>
    <!--=====================================-->
    <article class="row margin-top-2">
      <table class="col s6 offset-s3 table-lab striped centered">
        <tbody>
          <tr>
            <td>Leucocitos</td>
            <td>7610</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>Neutrófilos</td>
            <td>5100</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>Leucocitos</td>
            <td>1620</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>Eosinófilos</td>
            <td>10</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>Basófilos</td>
            <td>20</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>Monocitos</td>
            <td>560</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>Hemoglobina</td>
            <td>10.2 g/dL</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>Hematocrito</td>
            <td>32%</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>MCV</td>
            <td>95 fL</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>MCH</td>
            <td>30.2 pg</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>RDW</td>
            <td>15.9%</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>Plaquetas</td>
            <td>230.000</td>
          </tr>
        </tbody>
      </table>
    </article>
    <!--=====================================-->
    <article class="row margin-top-2">
      <table class=" col s6 offset-s3 table-lab striped centered">
        <tbody>
          <tr>
            <td>Troponina I:</td>
            <td>0.08 ng/mL (0-0.4)</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>CKMB:</td>
            <td>1.4 ng/mL (0-4.3)</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>PCR</td>
            <td>8.0 mg/dL</td>
          </tr>
        </tbody>
      </table>
    </article>
    <!--=====================================-->
    <article class="row margin-top-2">
      <table class="col s6 offset-s3 table-lab striped centered">
        <tbody>
          <tr>
            <td>Creatinina:</td>
            <td>0.99 mg/dL</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>BUN:</td>
            <td>20.4 mg/dL</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>Na</td>
            <td>146 mEq/L</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>K</td>
            <td>4.8 mEq/L</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>Cl:</td>
            <td>108 mEq/L</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td>Ácido láctico:</td>
            <td>1.2 mmol/L</td>
          </tr>
        </tbody>
      </table>
    </article>
    <!--=====================================-->
    <article class="row margin-top-2">
      <table class="col s6 offset-s3 table-lab striped centered">
        <tbody>
          <tr>
            <td> TSH: </td>
            <td>1.65 mUI/mL</td>
          </tr>
          <!--=====================================-->
          <tr>
            <td> T4L: </td>
            <td>0.90 ng/dL</td>
          </tr>
        </tbody>
      </table>
    </article>
    <!--=====================================-->
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Cerrar</a>
  </div>
</div>
<!--=====================================-->
<!--=====================================-->
<!--=====================================-->
<!-- Modal Structure Ant -->
<div id="modal-ant" class="modal modal-fixed-footer">
  <div class="modal-content center-align">
    <h4>ANTECEDENTES</h4>
    <!--=====================================-->
    <!--PERSONAL BACKGROUND-->
    <article class="row">
      <div class=" col s12 padding-l-r-1">
        <div class=" col s12 m12  z-depth-2">
          <!--=====================================-->
          <figure class="col s12 fis-img-wrapper">
            <!--<img src="<?php echo bloginfo('url') . '/wp-content/uploads/2017/05/piel.svg' ?>" width="80">-->
          </figure>
          <!--=====================================-->
          <div class="col s12 title-card margin-top 1 center-align">
            <h4 class="card-title">PERSONALES:</h4>
          </div>
          <!--============================-->
          <!--============================-->
          <div class="col s12 title-card margin-top 2 center-align">
            <h5 class="card-title">PATOLÓGICOS:</h5>
            <p class="margin-top-1">
              Anemia ferropénica diagnosticada en embarazo. Infección por Varicella zoster a los 10 años de edad?, con compromiso de III y IV par craneales izquierdos.
            </p>
          </div>
          <!--============================-->
          <!--============================-->
          <div class="col s12 title-card margin-top 2 center-align">
            <h5 class="card-title">FARMACOLOGICOS:</h5>
            <p class="margin-top-1">
              Fumarato ferroso 60mg/ácido ascórbico 70mg/ácido fólico 0.4mg tab. cada 12 horas, carbonato de calcio tab. 600 mg cada 12 horas.
            </p>
          </div>
          <!--============================-->
          <!--============================-->
          <div class="col s12 title-card margin-top 2 center-align">
            <h5 class="card-title">QUIRÚRGICOS:</h5>
            <p class="margin-top-1">
              Cirugía del elevador del párpado izquierdo hace 7 años
            </p>
          </div>
          <!--============================-->
          <!--============================-->
          <div class="col s12 title-card margin-top 2 center-align">
            <h5 class="card-title">ALÉRGICO/TÓXICOS:</h5>
            <p class="margin-top-1">
              Niega consumo de alcohol y/o tabaco. Niega consumo de medicaciones naturistas o herbales.

            </p>
          </div>
          <!--============================-->
          <!--============================-->
          <div class="col s12 title-card margin-top 2 center-align">
            <h5 class="card-title">TRAUMÁTICOS:</h5>
            <p class="margin-top-1">
              Niega
            </p>
          </div>
          <!--============================-->
          <!--============================-->
          <div class="col s12 title-card margin-top 2 center-align">
            <h5 class="card-title">TRANSFUSIONALES:</h5>
            <p class="margin-top-1">
              Niega
            </p>
          </div>
          <!--============================-->
          <!--============================-->
          <div class="col s12 title-card margin-top 2 center-align">
            <h5 class="card-title">GINECOBSTÉTRICOS:</h5>
            <p class="margin-top-1">
              G1P0. Menarca 11 años, sexarca 17 años, no planifica. Ciclos regulares 28X4. Controles prenatales: 4.
            </p>
          </div>
        </div>
        <!--=====================================-->
        <!--Close div with padding-->
      </div>
    </article>
    <!--=====================================-->
    <!--FAMILY BACKGROUND-->
    <article class="row">
      <div class=" col s12 padding-l-r-1">
        <div class=" col s12 m12  z-depth-2">
          <!--=====================================-->
          <figure class="col s12 fis-img-wrapper">
            <!--<img src="<?php echo bloginfo('url') . '/wp-content/uploads/2017/05/piel.svg' ?>" width="80">-->
          </figure>
          <!--=====================================-->
          <div class="col s12 title-card margin-top 1 center-align">
            <h4 class="card-title">FAMILIARES:</h4>
            <p class="margin-top-1">
              <p class="margin-top-1">
                Anemia ferropénica diagnosticada en embarazo. Infección por Varicella zoster a los 10 años de edad?, con compromiso de III y IV par craneales izquierdos.
              </p>
            </p>
          </div>
          <!--=====================================-->
          <!--Close div with padding-->
        </div>
    </article>
    <!--=====================================-->
    <!--SYSTEM REVISION-->
    <article class="row">
      <div class=" col s12 padding-l-r-1">
        <div class=" col s12 m12  z-depth-2">
          <!--=====================================-->
          <figure class="col s12 fis-img-wrapper">
            <!--<img src="<?php echo bloginfo('url') . '/wp-content/uploads/2017/05/piel.svg' ?>" width="80">-->
          </figure>
          <!--=====================================-->
          <div class="col s12 title-card margin-top 1 center-align">
            <h4 class="card-title">REVISIÓN POR SISTEMAS:</h4>
            <p class="margin-top-1">
              <p class="margin-top-1">
                Niega síntomas secos como xerostomía, xeroftalmia. Niega síntomas articulares, cutáneos, genitourinarios o neurológicos. Presentó cuadro de hiperdefecación, no disentérica, hace 5 días, que se autolimitó. Desde Noviembre de 2016, se ha documentado en
                controles prenatales ganancia de peso de 2Kg.
              </p>
            </p>
          </div>
          <!--=====================================-->
          <!--Close div with padding-->
        </div>
    </article>
    <!--=====================================-->
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Cerrar</a>
    </div>
    </div>