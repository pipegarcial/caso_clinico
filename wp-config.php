<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'clinical_case');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'Bl;{~e3iZ-1*PRieE5$-717.<7[c]d5@:HgE&vgqfocu}:`r2.iZ.R^af~&z<g08');
define('SECURE_AUTH_KEY', 'VM7c]>q=Q3=aL+8+%8X0d5G67Vcn3o;D60wa,U7^DoxLBw/zI`T]aF&&5FukV28}');
define('LOGGED_IN_KEY', '`DJ4Z^hI:8%^rKt(m$Mx?auUdn6q*$Thd%$g.*QO=CeWT!jUwUHiN$i;7n%p{s*F');
define('NONCE_KEY', '@RQFk,=hdRfT2(<CjOU%AUW(0t$5GspokIgeIeE~hc?.vSqhsRb4kx2E4LnWSUGx');
define('AUTH_SALT', '$q]z<Tc~bsPy4)#=ugP!N|>7 rlU_!n6;+s1u[;I26ip>95+S9r>gz `7W38Y$u2');
define('SECURE_AUTH_SALT', '7t%kknv[Cp2a2/F0SdzU.+F!_|-U<*MQt_|<xC*10ysoIoA&wuk#}4,[YeU!$#yZ');
define('LOGGED_IN_SALT', 'RFtZ8OQu$yCKjs>//%}gE*c#>7^+/,>0Z.54M;a^wxd`}.>ExDi_EIun4L^9.FH5');
define('NONCE_SALT', 'Pjz,W*w>v59tZ_;}yqx9UP!D~)d}w5&cJ+{o}Dw]@ Js?6+{Z>4JeanJqDF1raL#');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


